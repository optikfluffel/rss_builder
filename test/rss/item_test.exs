defmodule RSS.ItemTest do
  use ExUnit.Case, async: true

  alias RSS.Item
  alias RSS.Item.{Author, Category, Enclosure, Guid, Source}

  setup do
    item = %Item{title: "Test Item Title"}

    {:ok, base_item: item}
  end

  describe "to_elements/1" do
    test "works with title field only", %{base_item: item} do
      assert Item.to_elements(item) == {:item, nil, [{:title, nil, "Test Item Title"}]}
    end

    test "works with description field only" do
      item = %Item{description: "Test Item Description"}

      assert Item.to_elements(item) ==
               {:item, nil, [{:description, nil, "Test Item Description"}]}
    end

    test "errors without title or description" do
      item = %Item{}

      assert_raise ArgumentError, "either title or description have to be set", fn ->
        Item.to_elements(item)
      end
    end

    test "adds optional link", %{base_item: base_item} do
      item = Map.put(base_item, :link, "https://non.existing.tld/news/hello_internet")

      assert {:item, nil, contents} = Item.to_elements(item)
      assert Enum.member?(contents, {:link, nil, "https://non.existing.tld/news/hello_internet"})
    end

    test "adds optional link with %URI{}", %{base_item: base_item} do
      url = URI.parse("https://non.existing.tld/news/hello_internet")
      item = Map.put(base_item, :link, url)

      assert {:item, nil, contents} = Item.to_elements(item)
      assert Enum.member?(contents, {:link, nil, "https://non.existing.tld/news/hello_internet"})
    end

    test "adds optional description", %{base_item: base_item} do
      item = Map.put(base_item, :description, "Basically a hello world.")

      assert {:item, nil, contents} = Item.to_elements(item)
      assert Enum.member?(contents, {:description, nil, "Basically a hello world."})
    end

    test "adds optional author", %{base_item: base_item} do
      author = %Author{email: "hopper@non.existing.tld"}
      item = Map.put(base_item, :author, author)

      assert {:item, nil, contents} = Item.to_elements(item)
      assert Enum.member?(contents, {:author, nil, "hopper@non.existing.tld"})
    end

    test "adds optional category", %{base_item: base_item} do
      category = %Category{title: "Heavy Metal"}
      item = Map.put(base_item, :category, category)

      assert {:item, nil, contents} = Item.to_elements(item)
      assert Enum.member?(contents, {:category, nil, "Heavy Metal"})
    end

    test "adds optional category with list of categories", %{base_item: item} do
      categories = [%Category{title: "Rock"}, %Category{title: "Metal"}]
      item = Map.put(item, :category, categories)

      assert {:item, nil, contents} = Item.to_elements(item)
      assert Enum.member?(contents, [{:category, nil, "Rock"}, {:category, nil, "Metal"}])
    end

    test "adds optional comments", %{base_item: base_item} do
      item =
        Map.put(base_item, :comments, "https://non.existing.tld/news/hello_internet#comments")

      assert {:item, nil, contents} = Item.to_elements(item)

      assert Enum.member?(
               contents,
               {:comments, nil, "https://non.existing.tld/news/hello_internet#comments"}
             )
    end

    test "adds optional comments with %URI{}", %{base_item: base_item} do
      url = URI.parse("https://non.existing.tld/news/hello_internet#comments")
      item = Map.put(base_item, :comments, url)

      assert {:item, nil, contents} = Item.to_elements(item)

      assert Enum.member?(
               contents,
               {:comments, nil, "https://non.existing.tld/news/hello_internet#comments"}
             )
    end

    test "adds optional enclosure", %{base_item: base_item} do
      enclosure = %Enclosure{
        url: "https://non.existing.tld/news/spoken/blm.mp3",
        length: 20_480,
        type: "audio/mpeg"
      }

      item = Map.put(base_item, :enclosure, enclosure)

      assert {:item, nil, contents} = Item.to_elements(item)

      assert Enum.member?(
               contents,
               {:enclosure,
                %{
                  url: "https://non.existing.tld/news/spoken/blm.mp3",
                  length: "20480",
                  type: "audio/mpeg"
                }, nil}
             )
    end

    test "adds optional guid", %{base_item: base_item} do
      guid = %Guid{value: "https://non.existing.tld/news/8734269823156783"}

      item = Map.put(base_item, :guid, guid)

      assert {:item, nil, contents} = Item.to_elements(item)

      assert Enum.member?(
               contents,
               {:guid, %{isPermaLink: true}, "https://non.existing.tld/news/8734269823156783"}
             )
    end

    test "adds optional pub_date with %NaiveDateTime{}", %{base_item: item} do
      item = Map.put(item, :pub_date, NaiveDateTime.utc_now())

      assert {:item, nil, contents} = Item.to_elements(item)

      assert {:pubDate, nil, content} =
               contents |> Enum.find(fn {key, _attrs, _content} -> key == :pubDate end)

      assert String.length(content) > 0
    end

    test "adds optional pub_date with %DateTime{}", %{base_item: item} do
      item = Map.put(item, :pub_date, DateTime.utc_now())

      assert {:item, nil, contents} = Item.to_elements(item)

      assert {:pubDate, nil, content} =
               contents |> Enum.find(fn {key, _attrs, _content} -> key == :pubDate end)

      assert String.length(content) > 0
    end

    test "adds optional source", %{base_item: base_item} do
      source = %Source{url: "https://another.non.existing.tld/feed.xml"}
      item = Map.put(base_item, :source, source)

      assert {:item, nil, contents} = Item.to_elements(item)

      assert Enum.member?(
               contents,
               {:source, %{url: "https://another.non.existing.tld/feed.xml"}, nil}
             )
    end
  end
end
