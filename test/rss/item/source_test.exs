defmodule RSS.Item.SourceTest do
  use ExUnit.Case, async: true

  alias RSS.Item.Source

  describe "to_elements/1" do
    test "works with complete %Source{}" do
      source = %Source{
        url: "https://another.non.existing.tld/feed.xml",
        title: "A completely different Realm"
      }

      assert Source.to_elements(source) ==
               {:source, %{url: "https://another.non.existing.tld/feed.xml"},
                "A completely different Realm"}
    end

    test "works without title" do
      source = %Source{url: "https://another.non.existing.tld/feed.xml"}

      assert Source.to_elements(source) ==
               {:source, %{url: "https://another.non.existing.tld/feed.xml"}, nil}
    end

    test "works with an %URI{}" do
      url = URI.parse("https://another.non.existing.tld/feed.xml")
      source = %Source{url: url, title: "A completely different Realm"}

      assert Source.to_elements(source) ==
               {:source, %{url: "https://another.non.existing.tld/feed.xml"},
                "A completely different Realm"}
    end

    test "works with an %URI{} without a title" do
      url = URI.parse("https://another.non.existing.tld/feed.xml")
      source = %Source{url: url}

      assert Source.to_elements(source) ==
               {:source, %{url: "https://another.non.existing.tld/feed.xml"}, nil}
    end
  end
end
