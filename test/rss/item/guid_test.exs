defmodule RSS.Item.GuidTest do
  use ExUnit.Case, async: true

  alias RSS.Item.Guid

  describe "to_elements/1" do
    test "works with complete %Guid{}" do
      guid = %Guid{
        value: "https://non.existing.tld/news/32849238749",
        is_perma_link: true
      }

      assert Guid.to_elements(guid) ==
               {:guid, %{isPermaLink: true}, "https://non.existing.tld/news/32849238749"}
    end

    test "works with is_perma_link: false" do
      guid = %Guid{
        value: "https://non.existing.tld/news/32849238749",
        is_perma_link: false
      }

      assert Guid.to_elements(guid) == {:guid, nil, "https://non.existing.tld/news/32849238749"}
    end

    test "is_perma_link is true by default" do
      guid = %Guid{value: "https://non.existing.tld/news/32849238749"}

      assert Guid.to_elements(guid) ==
               {:guid, %{isPermaLink: true}, "https://non.existing.tld/news/32849238749"}
    end
  end
end
