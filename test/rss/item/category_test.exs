defmodule RSS.Item.CategoryTest do
  use ExUnit.Case, async: true

  alias RSS.Item.Category

  describe "to_elements/1" do
    test "works with complete %Category{}" do
      category = %Category{
        title: "Movie Reviews",
        domain: "https://non.existing.tld/news/tags/movie_reviews"
      }

      assert Category.to_elements(category) ==
               {:category, %{domain: "https://non.existing.tld/news/tags/movie_reviews"},
                "Movie Reviews"}
    end

    test "works without domain" do
      category = %Category{title: "Movie Reviews"}

      assert Category.to_elements(category) ==
               {:category, nil, "Movie Reviews"}
    end

    test "works with an %URI{}" do
      domain = URI.parse("https://non.existing.tld/news/tags/movie_reviews")
      category = %Category{title: "Movie Reviews", domain: domain}

      assert Category.to_elements(category) ==
               {:category, %{domain: "https://non.existing.tld/news/tags/movie_reviews"},
                "Movie Reviews"}
    end
  end
end
