defmodule RSS.Item.AuthorTest do
  use ExUnit.Case, async: true

  alias RSS.Item.Author

  describe "to_elements/1" do
    test "works with complete %Author{}" do
      author = %Author{
        email: "hamilton@non.existing.tld",
        name: "Margaret Heafield Hamilton"
      }

      assert Author.to_elements(author) ==
               {:author, nil, "hamilton@non.existing.tld (Margaret Heafield Hamilton)"}
    end

    test "works without name" do
      author = %Author{email: "hamilton@non.existing.tld"}

      assert Author.to_elements(author) == {:author, nil, "hamilton@non.existing.tld"}
    end
  end
end
