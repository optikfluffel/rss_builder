defmodule RSS.Item.EnclosureTest do
  use ExUnit.Case, async: true

  alias RSS.Item.Enclosure

  describe "to_elements/1" do
    test "works with complete %Enclosure{}" do
      enclosure = %Enclosure{
        url: "https://non.existing.tld/news/spoken/nullnummer.mp3",
        length: 10_240,
        type: "audio/mpeg"
      }

      assert Enclosure.to_elements(enclosure) ==
               {
                 :enclosure,
                 %{
                   length: "10240",
                   type: "audio/mpeg",
                   url: "https://non.existing.tld/news/spoken/nullnummer.mp3"
                 },
                 nil
               }
    end

    test "works with %URI{}" do
      url = URI.parse("https://non.existing.tld/news/spoken/nullnummer.mp3")

      enclosure = %Enclosure{
        url: url,
        length: 10_240,
        type: "audio/mpeg"
      }

      assert Enclosure.to_elements(enclosure) ==
               {
                 :enclosure,
                 %{
                   length: "10240",
                   type: "audio/mpeg",
                   url: "https://non.existing.tld/news/spoken/nullnummer.mp3"
                 },
                 nil
               }
    end
  end
end
