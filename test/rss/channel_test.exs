defmodule RSS.ChannelTest do
  use ExUnit.Case, async: true

  alias RSS.Channel
  alias RSS.Channel.{Cloud, Image, ManagingEditor, TextInput, WebMaster}

  setup do
    channel = %Channel{
      title: "Test Channel Title",
      link: "https://non.existing.tld/news",
      description: "RSS channel without any meaningful content."
    }

    {:ok, base_channel: channel}
  end

  describe "to_elements/1" do
    test "works with required keys only", %{base_channel: channel} do
      expected = [
        {:description, nil, "RSS channel without any meaningful content."},
        {:link, nil, "https://non.existing.tld/news"},
        {:title, nil, "Test Channel Title"}
      ]

      assert channel |> Channel.to_elements() |> Enum.all?(fn e -> e in expected end)
    end

    test "works with link as %URI{}", %{base_channel: channel} do
      url = URI.parse("https://non.existing.tld/news")
      channel = Map.put(channel, :link, url)

      assert channel
             |> Channel.to_elements()
             |> Enum.member?({:link, nil, "https://non.existing.tld/news"})
    end

    test "adds optional language", %{base_channel: channel} do
      channel = Map.put(channel, :language, "de")

      assert channel |> Channel.to_elements() |> Enum.member?({:language, nil, "de"})
    end

    test "adds optional copyright", %{base_channel: channel} do
      channel = Map.put(channel, :copyright, "No Rights Reserved")

      assert channel
             |> Channel.to_elements()
             |> Enum.member?({:copyright, nil, "No Rights Reserved"})
    end

    test "adds optional managing_editor", %{base_channel: channel} do
      managing_editor = %ManagingEditor{email: "vaughan@non.existing.tld"}
      channel = Map.put(channel, :managing_editor, managing_editor)

      assert channel
             |> Channel.to_elements()
             |> Enum.member?({:managingEditor, nil, "vaughan@non.existing.tld"})
    end

    test "adds optional web_master", %{base_channel: channel} do
      web_master = %WebMaster{email: "countess@non.existing.tld"}
      channel = Map.put(channel, :web_master, web_master)

      assert channel
             |> Channel.to_elements()
             |> Enum.member?({:webMaster, nil, "countess@non.existing.tld"})
    end

    test "adds optional pub_date with %NaiveDateTime{}", %{base_channel: channel} do
      channel = Map.put(channel, :pub_date, NaiveDateTime.utc_now())

      assert {:pubDate, nil, content} =
               channel
               |> Channel.to_elements()
               |> Enum.find(fn {key, _attrs, _content} -> key == :pubDate end)

      assert String.length(content) > 0
    end

    test "adds optional pub_date with %DateTime{}", %{base_channel: channel} do
      channel = Map.put(channel, :pub_date, DateTime.utc_now())

      assert {:pubDate, nil, content} =
               channel
               |> Channel.to_elements()
               |> Enum.find(fn {key, _attrs, _content} -> key == :pubDate end)

      assert String.length(content) > 0
    end

    test "adds optional last_build_date with %NaiveDateTime{}", %{base_channel: channel} do
      channel = Map.put(channel, :last_build_date, NaiveDateTime.utc_now())

      assert {:lastBuildDate, nil, content} =
               channel
               |> Channel.to_elements()
               |> Enum.find(fn {key, _attrs, _content} -> key == :lastBuildDate end)

      assert String.length(content) > 0
    end

    test "adds optional last_build_date with %DateTime{}", %{base_channel: channel} do
      channel = Map.put(channel, :last_build_date, DateTime.utc_now())

      assert {:lastBuildDate, nil, content} =
               channel
               |> Channel.to_elements()
               |> Enum.find(fn {key, _attrs, _content} -> key == :lastBuildDate end)

      assert String.length(content) > 0
    end

    test "adds optional category", %{base_channel: channel} do
      channel = Map.put(channel, :category, "Geology")

      assert channel
             |> Channel.to_elements()
             |> Enum.member?({:category, nil, "Geology"})
    end

    test "adds optional category with list of categories", %{base_channel: channel} do
      channel = Map.put(channel, :category, ["Rock", "Metal"])

      assert channel
             |> Channel.to_elements()
             |> Enum.member?([{:category, nil, "Rock"}, {:category, nil, "Metal"}])
    end

    test "adds optional generator", %{base_channel: channel} do
      channel = Map.put(channel, :generator, "Fancy Phoenix CMS 0.1")

      assert channel
             |> Channel.to_elements()
             |> Enum.member?({:generator, nil, "Fancy Phoenix CMS 0.1"})
    end

    test "adds optional docs", %{base_channel: channel} do
      channel = Map.put(channel, :docs, "https://www.rssboard.org/rss-specification")

      assert channel
             |> Channel.to_elements()
             |> Enum.member?({:docs, nil, "https://www.rssboard.org/rss-specification"})
    end

    test "adds optional docs as %URI{}", %{base_channel: channel} do
      url = URI.parse("https://www.rssboard.org/rss-specification")
      channel = Map.put(channel, :docs, url)

      assert channel
             |> Channel.to_elements()
             |> Enum.member?({:docs, nil, "https://www.rssboard.org/rss-specification"})
    end

    test "adds optional cloud", %{base_channel: channel} do
      cloud = %Cloud{
        domain: "non.existing.tld",
        port: 1984,
        path: "/rpc",
        register_procedure: "pleasePingMe",
        protocol: "xml-rpc"
      }

      channel = Map.put(channel, :cloud, cloud)

      assert channel
             |> Channel.to_elements()
             |> Enum.member?(
               {:cloud,
                %{
                  domain: "non.existing.tld",
                  path: "/rpc",
                  port: 1984,
                  protocol: "xml-rpc",
                  registerProcedure: "pleasePingMe"
                }, nil}
             )
    end

    test "adds optional ttl", %{base_channel: channel} do
      channel = Map.put(channel, :ttl, 42)

      assert channel |> Channel.to_elements() |> Enum.member?({:ttl, nil, "42"})
    end

    test "adds optional image", %{base_channel: channel} do
      image = %Image{
        url: "https://non.existing.tld/images/news.jpg",
        title: "An icon resembling a newspaper.",
        link: "https://non.existing.tld/news"
      }

      channel = Map.put(channel, :image, image)

      assert channel
             |> Channel.to_elements()
             |> Enum.member?({
               :image,
               nil,
               [
                 {:link, nil, "https://non.existing.tld/news"},
                 {:title, nil, "An icon resembling a newspaper."},
                 {:url, nil, "https://non.existing.tld/images/news.jpg"}
               ]
             })
    end

    test "adds optional rating", %{base_channel: channel} do
      channel = Map.put(channel, :rating, "FSK 16")

      assert channel |> Channel.to_elements() |> Enum.member?({:rating, nil, "FSK 16"})
    end

    test "adds optional text_input", %{base_channel: channel} do
      text_input = %TextInput{
        title: "Submit Feedback",
        description: "Provide feedback regarding our awesome website.",
        name: "content",
        link: "https://non.existing.tld/feedback"
      }

      channel = Map.put(channel, :text_input, text_input)

      assert channel
             |> Channel.to_elements()
             |> Enum.member?(
               {:text_input, nil,
                [
                  {:title, nil, "Submit Feedback"},
                  {:description, nil, "Provide feedback regarding our awesome website."},
                  {:name, nil, "content"},
                  {:link, nil, "https://non.existing.tld/feedback"}
                ]}
             )
    end

    test "adds optional skip_hours", %{base_channel: channel} do
      channel = Map.put(channel, :skip_hours, [3, 4, 5])

      assert channel
             |> Channel.to_elements()
             |> Enum.member?(
               {:skip_hours, nil,
                [
                  {:hour, nil, "3"},
                  {:hour, nil, "4"},
                  {:hour, nil, "5"}
                ]}
             )
    end

    test "adds optional skip_days", %{base_channel: channel} do
      channel = Map.put(channel, :skip_days, [:monday, :friday])

      assert channel
             |> Channel.to_elements()
             |> Enum.member?(
               {:skip_days, nil,
                [
                  {:day, nil, "Monday"},
                  {:day, nil, "Friday"}
                ]}
             )
    end

    test "adds optional atom_link", %{base_channel: channel} do
      channel = Map.put(channel, :atom_link, "https://non.existing.tld/news/feed")

      assert channel
             |> Channel.to_elements()
             |> Enum.member?(
               {:"atom:link",
                %{
                  href: "https://non.existing.tld/news/feed",
                  rel: "self",
                  type: "application/rss+xml"
                }, nil}
             )
    end

    test "adds optional %URI{} atom_link", %{base_channel: channel} do
      uri = URI.parse("https://non.existing.tld/news/feed")
      channel = Map.put(channel, :atom_link, uri)

      assert channel
             |> Channel.to_elements()
             |> Enum.member?(
               {:"atom:link",
                %{
                  href: "https://non.existing.tld/news/feed",
                  rel: "self",
                  type: "application/rss+xml"
                }, nil}
             )
    end
  end
end
