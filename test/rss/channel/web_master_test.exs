defmodule RSS.Channel.WebMasterTest do
  use ExUnit.Case, async: true

  alias RSS.Channel.WebMaster

  describe "to_elements/1" do
    test "works with complete %WebMaster{}" do
      web_master = %WebMaster{
        email: "caerostris@non.existing.tld",
        name: "Caerostris darwini"
      }

      assert WebMaster.to_elements(web_master) ==
               {:webMaster, nil, "caerostris@non.existing.tld (Caerostris darwini)"}
    end

    test "works without name" do
      web_master = %WebMaster{email: "caerostris@non.existing.tld"}

      assert WebMaster.to_elements(web_master) ==
               {:webMaster, nil, "caerostris@non.existing.tld"}
    end
  end
end
