defmodule RSS.Channel.ManagingEditorTest do
  use ExUnit.Case, async: true

  alias RSS.Channel.ManagingEditor

  describe "to_elements/1" do
    test "works with complete %ManagingEditor{}" do
      web_master = %ManagingEditor{
        email: "vaughan@non.existing.tld",
        name: "Dorothy Vaughan"
      }

      assert ManagingEditor.to_elements(web_master) ==
               {:managingEditor, nil, "vaughan@non.existing.tld (Dorothy Vaughan)"}
    end

    test "works without name" do
      web_master = %ManagingEditor{email: "vaughan@non.existing.tld"}

      assert ManagingEditor.to_elements(web_master) ==
               {:managingEditor, nil, "vaughan@non.existing.tld"}
    end
  end
end
