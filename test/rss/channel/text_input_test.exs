defmodule RSS.Channel.TextInputTest do
  use ExUnit.Case, async: true

  alias RSS.Channel.TextInput

  describe "to_elements/1" do
    test "works with complete %TextInput{}" do
      text_input = %TextInput{
        title: "Submit Feedback",
        description: "Provide feedback regarding our awesome website.",
        name: "content",
        link: "https://non.existing.tld/feedback"
      }

      assert TextInput.to_elements(text_input) ==
               {
                 :text_input,
                 nil,
                 [
                   {:title, nil, "Submit Feedback"},
                   {:description, nil, "Provide feedback regarding our awesome website."},
                   {:name, nil, "content"},
                   {:link, nil, "https://non.existing.tld/feedback"}
                 ]
               }
    end

    test "works with %URI{}" do
      link = URI.parse("https://non.existing.tld/feedback")

      text_input = %TextInput{
        title: "Submit Feedback",
        description: "Provide feedback regarding our awesome website.",
        name: "content",
        link: link
      }

      assert TextInput.to_elements(text_input) ==
               {
                 :text_input,
                 nil,
                 [
                   {:title, nil, "Submit Feedback"},
                   {:description, nil, "Provide feedback regarding our awesome website."},
                   {:name, nil, "content"},
                   {:link, nil, "https://non.existing.tld/feedback"}
                 ]
               }
    end
  end
end
