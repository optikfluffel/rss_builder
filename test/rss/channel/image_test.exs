defmodule RSS.Channel.ImageTest do
  use ExUnit.Case, async: true

  alias RSS.Channel.Image

  describe "to_elements/1" do
    test "works with complete %Image{}" do
      image = %Image{
        url: "https://non.existing.tld/images/news.jpg",
        title: "An icon resembling a newspaper.",
        link: "https://non.existing.tld/news",
        width: 120,
        height: 240,
        description: "A non existing News website"
      }

      assert {:image, nil, contents} = Image.to_elements(image)

      assert Enum.member?(contents, {:url, nil, "https://non.existing.tld/images/news.jpg"})
      assert Enum.member?(contents, {:title, nil, "An icon resembling a newspaper."})
      assert Enum.member?(contents, {:link, nil, "https://non.existing.tld/news"})
      assert Enum.member?(contents, {:width, nil, "120"})
      assert Enum.member?(contents, {:height, nil, "240"})
      assert Enum.member?(contents, {:description, nil, "A non existing News website"})
    end

    test "works with %URI{} instead of strings" do
      url = URI.parse("https://non.existing.tld/images/news.jpg")
      link = URI.parse("https://non.existing.tld/news")

      image = %Image{
        url: url,
        title: "An icon resembling a newspaper.",
        link: link
      }

      assert Image.to_elements(image) == {
               :image,
               nil,
               [
                 {:link, nil, "https://non.existing.tld/news"},
                 {:title, nil, "An icon resembling a newspaper."},
                 {:url, nil, "https://non.existing.tld/images/news.jpg"}
               ]
             }
    end

    test "works with required keys only" do
      image = %Image{
        url: "https://non.existing.tld/images/news.jpg",
        title: "An icon resembling a newspaper.",
        link: "https://non.existing.tld/news"
      }

      assert Image.to_elements(image) == {
               :image,
               nil,
               [
                 {:link, nil, "https://non.existing.tld/news"},
                 {:title, nil, "An icon resembling a newspaper."},
                 {:url, nil, "https://non.existing.tld/images/news.jpg"}
               ]
             }
    end

    test "respects height boundaries" do
      image = %Image{
        url: "https://non.existing.tld/images/news.jpg",
        title: "An icon resembling a newspaper.",
        link: "https://non.existing.tld/news",
        width: 120
      }

      assert_raise ArgumentError, "height for an Image should be 1-400 (pixel)", fn ->
        Image.to_elements(%{image | height: 0})
      end

      assert {:image, nil, _foo} = Image.to_elements(%{image | height: 1})
      assert {:image, nil, _foo} = Image.to_elements(%{image | height: 400})

      assert_raise ArgumentError, "height for an Image should be 1-400 (pixel)", fn ->
        Image.to_elements(%{image | height: 401})
      end
    end

    test "respects width boundaries" do
      image = %Image{
        url: "https://non.existing.tld/images/news.jpg",
        title: "An icon resembling a newspaper.",
        link: "https://non.existing.tld/news",
        height: 240
      }

      assert_raise ArgumentError, "width for an Image should be 1-144 (pixel)", fn ->
        Image.to_elements(%{image | width: 0})
      end

      assert {:image, nil, _foo} = Image.to_elements(%{image | width: 1})
      assert {:image, nil, _foo} = Image.to_elements(%{image | width: 144})

      assert_raise ArgumentError, "width for an Image should be 1-144 (pixel)", fn ->
        Image.to_elements(%{image | width: 145})
      end
    end
  end
end
