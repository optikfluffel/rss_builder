defmodule RSS.Channel.CloudTest do
  use ExUnit.Case, async: true

  alias RSS.Channel.Cloud

  test "to_elements/1 works with complete %Cloud{}" do
    cloud = %Cloud{
      domain: "non.existing.tld",
      port: 1984,
      path: "/rpc",
      register_procedure: "pleasePingMe",
      protocol: "xml-rpc"
    }

    assert Cloud.to_elements(cloud) ==
             {:cloud,
              %{
                domain: "non.existing.tld",
                path: "/rpc",
                port: 1984,
                protocol: "xml-rpc",
                registerProcedure: "pleasePingMe"
              }, nil}
  end
end
