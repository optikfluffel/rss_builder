defmodule RSSTest do
  use ExUnit.Case, async: true
  # doctest RSS

  describe "feed/2" do
    test "outputs at least some xml" do
      channel = %RSS.Channel{
        title: "Test Channel Title",
        link: "https://non.existing.tld/news",
        description: "RSS channel without any meaningful content."
      }

      item = %RSS.Item{title: "A Test Item"}

      assert {:ok, xml_content} = RSS.feed(channel, [item])

      assert xml_content <> "\n" == """
             <?xml version=\"1.0\" encoding=\"UTF-8\"?>
             <rss version=\"2.0\" xmlns:atom=\"http://www.w3.org/2005/Atom\">
               <channel>
                 <link>https://non.existing.tld/news</link>
                 <description>RSS channel without any meaningful content.</description>
                 <title>Test Channel Title</title>
                 <item>
                   <title>A Test Item</title>
                 </item>
               </channel>
             </rss>
             """
    end

    test "outputs some xml without indentation" do
      channel = %RSS.Channel{
        title: "Test Channel Title Without Indentation",
        link: "https://non.existing.tld/news",
        description: "RSS channel without any meaningful content."
      }

      item = %RSS.Item{title: "A Test Item"}

      assert {:ok, xml_content} = RSS.feed(channel, [item], :no_indent)

      assert xml_content ==
               Enum.join([
                 "<?xml version=\"1.0\" encoding=\"UTF-8\"?>",
                 "<rss version=\"2.0\" xmlns:atom=\"http://www.w3.org/2005/Atom\">",
                 "<channel>",
                 "<link>https://non.existing.tld/news</link>",
                 "<description>RSS channel without any meaningful content.</description>",
                 "<title>Test Channel Title Without Indentation</title>",
                 "<item>",
                 "<title>A Test Item</title>",
                 "</item>",
                 "</channel>",
                 "</rss>"
               ])
    end
  end
end
