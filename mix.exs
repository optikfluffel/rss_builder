defmodule RSS.MixProject do
  use Mix.Project

  def project do
    [
      app: :rss_builder,
      version: "0.3.28",
      elixir: "~> 1.11",
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      source_url: "https://gitlab.com/optikfluffel/rss_builder",
      docs: [
        main: "RSS",
        groups_for_modules: [
          Item: [
            RSS.Item,
            RSS.Item.Author,
            RSS.Item.Category,
            RSS.Item.Enclosure,
            RSS.Item.Guid,
            RSS.Item.Source
          ],
          Channel: [
            RSS.Channel,
            RSS.Channel.Cloud,
            RSS.Channel.Image,
            RSS.Channel.ManagingEditor,
            RSS.Channel.TextInput,
            RSS.Channel.WebMaster
          ],
          Internal: [
            RSS.Utilities
          ]
        ],
        nest_modules_by_prefix: [
          RSS.Item,
          RSS.Channel
        ]
      ],
      dialyzer: [
        flags: [
          :error_handling,
          :no_behaviours,
          :no_contracts,
          :no_fail_call,
          :no_fun_app,
          :no_improper_lists,
          :no_match,
          :no_missing_calls,
          :no_opaque,
          :no_return,
          :no_undefined_callbacks,
          :no_unused,
          # :overspecs,
          # :specdiffs,
          # :race_conditions,
          :underspecs,
          :unknown,
          :unmatched_returns
        ]
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: []
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:xml_builder, "~> 2.2"},
      {:dialyxir, "~> 1.2", only: [:dev], runtime: false},
      {:ex_doc, "~> 0.29", only: [:dev], runtime: false},
      {:credo, "~> 1.6", only: [:dev, :test], runtime: false},
      {:mix_audit, "~> 2.0", only: [:dev, :test], runtime: false}
    ]
  end

  defp aliases do
    [
      check: ["format --check-formatted", "dialyzer", "credo --strict", "deps.audit"]
    ]
  end
end
