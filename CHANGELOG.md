# Changelog

## `v0.3.28`

- Bump dependencies

## `v0.3.27`

- Bump dependencies

## `v0.3.26`

- Bump dependencies

## `v0.3.25`

- Bump dependencies

## `v0.3.24`

- Bump dependencies

## `v0.3.23`

- Bump dependencies

## `v0.3.22`

- Bump dependencies

## `v0.3.21`

- Bump dependencies

## `v0.3.20`

- Bump dependencies

## `v0.3.19`

- Bump `earmark_parser`

## `v0.3.18`

- Bump dependencies and tweak `Utilities.datetime_to_rfc1123/1`

## `v0.3.17`

- Bump `earmark_parser` and tweak docs

## `v0.3.16`

- Bump `ex_doc`

## `v0.3.15`

- Bump MixAudit to `1.0.1`

## `v0.3.14`

- Bump dependencies

## `v0.3.13`

- Bump dependencies

## `v0.3.12`

- Add [MixAudit](https://hex.pm/packages/mix_audit)

## `v0.3.11`

- Bump dependencies

## `v0.3.10`

- Bump `ex_doc`

## `v0.3.9`

- Bump `earmark_parser`, `ex_doc` and `jason`

## `v0.3.8`

- Bump `earmark_parser`

## `v0.3.7`

- Bump `credo`, `earmark_parser`, `ex_doc` and `nimble_parsec`

## `v0.3.6

- Bump `ex_doc`

## `v0.3.5`

- Removed `muzak`
- Updated dependencies
- Uses ExDoc's `groups_for_modules` and `nest_modules_by_prefix` options, to better organize modules in the sidebar

## `v0.3.1`

- Bump `earmark_parser`, `credo` and `xml_builder` dependencies

## `v0.3.0`

- Support list of `%RSS.Item.Category{}` in an `%RSS.Item{}`

## `v0.2.10`

- Either `title` or `description` are required for an Item (#3)
