# rss_builder

📡 An RSS feed builder, written in Elixir, using [`xml_builder`](https://github.com/joshnuss/xml_builder) for all the heavy lifting.

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `rss_builder` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:rss_builder, "~> 0.1.0"}
  ]
end
```

Once published, the docs can be found at [hexdocs.pm/rss_builder](https://hexdocs.pm/rss_builder),
but for now they're available at [optikfluffel.gitlab.io/rss_builder](https://optikfluffel.gitlab.io/rss_builder).

## Usage

```elixir
# prepare Channel struct
channel = %RSS.Channel{
  title: "Philosopher's Stone",
  link: "https://philosophers.stone/news",
  description: "It is also called the elixir of life."
}

# prepare a list of Item structs
items = [
  %RSS.Item{title: "Transmute Base Metals Into Gold or Silver"},
  %RSS.Item{title: "Heal All Forms of Illness"},
  %RSS.Item{title: "Reviving of Dead Plants"}
]

# generate xml feed
{:ok, feed_xml} = RSS.feed(channel, items)
```

For a Phoenix example and more information in general [check the docs](https://optikfluffel.gitlab.io/rss_builder).

## Development Tooling

```sh
# setup
mix deps.get
mix compile

# run unit tests
mix test

# run formatted check, dialyzer and credo
mix check
# run formatted check only
mix format --check-formatted
# run dialyzer only
mix dialyzer
# run credo only
mix credo --strict

# generate html docs
mix docs
# open docs in browser
open doc/index.html
```
