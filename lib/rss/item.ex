defmodule RSS.Item do
  @moduledoc """
  An Item struct represents a single `<item>` and it's metadata in the RSS channel.

  ## Required

  When creating an Item struct, either one of the `:title` or `:description` field is required.

  A minimal Item would look something like this:

      %RSS.Item{title: "Reviving of Dead Plants"}
      # or
      %RSS.Item{description: "A howto explaining how you never loose a houseplant again."}

  ## Optional

  There are also some optional fields you can set:

  ### `:link`

  The URL of the item.

  It can either be a `URI` or a `String`.

      item = %RSS.Item{
        # ...
        link: "https://non.existing.tld/news/hello_internet"
      }

  ### `:description`

  The item synopsis, for example the tldr; of a long blog post,
  but this could also include the whole content if the item.

      item = %RSS.Item{
        # ...
        description: "The item synopsis."
      }

  ### `:author`

  Email address of the author of the item.

  See `RSS.Item.Author` for details.

      item = %RSS.Item{
        # ...
        author: %Author{email: "hopper@non.existing.tld"}
      }

  ### `:category`

  Includes the item in one or more categories.

  See `RSS.Item.Category` for details.

      # a single category
      item = %RSS.Item{
        # ...
        category: %Category{title: "Heavy Metal"}
      }

      # or multiple categories
      item = %RSS.Item{
        # ...
        category: [%Category{title: "House"}, %Category{title: "Techno"}]
      }

  ### `:comments`

  URL of a page for comments relating to the item.

  It can either be a `URI` or a `String`.

      item = %RSS.Item{
        # ...
        comments: "https://non.existing.tld/news/hello_internet#comments"
      }

  ### `:enclosure`

  Describes a media object that is attached to the item.

  See `RSS.Item.Enclosure` for details.

      enclosure = %Enclosure{
        url: "https://non.existing.tld/news/spoken/blm.mp3",
        length: 20_480,
        type: "audio/mpeg"
      }

      item = %RSS.Item{
        # ...
        enclosure: enclosure
      }

  ### `:guid`

  A string that uniquely identifies the item.

  See `RSS.Item.Guid` for details.

      item = %RSS.Item{
        # ...
        guid: %Guid{value: "https://non.existing.tld/news/8734269823156783"}
      }

  ### `:pub_date`

  Indicates when the item was published, it can be a `NaiveDateTime` or `DateTime`.

      item = %RSS.Item{
        # ...
        pub_date: NaiveDateTime.utc_now()
      }

  ### `:source`

  The RSS channel that the item came from.

  See `RSS.Item.Source` for details.

      item = %RSS.Item{
        # ...
        source: %Source{url: "https://another.non.existing.tld/feed.xml"}
      }
  """

  import XmlBuilder, only: [element: 2]
  import RSS.Utilities, only: [datetime_to_rfc1123: 1, url_to_string: 1]

  alias RSS.Item.{Author, Category, Enclosure, Guid, Source}

  defstruct [
    :title,
    :link,
    :description,
    :author,
    :category,
    :comments,
    :enclosure,
    :guid,
    :pub_date,
    :source
  ]

  @typedoc """
  An Item struct represents a single `<item>` and it's metadata in the RSS channel.
  """
  @type t :: %__MODULE__{
          title: String.t(),
          link: URI.t() | String.t(),
          description: String.t(),
          author: Author.t(),
          category: Category.t(),
          comments: URI.t() | String.t(),
          enclosure: Enclosure.t(),
          guid: Guid.t(),
          pub_date: DateTime.t() | NaiveDateTime.t(),
          source: Source.t()
        }

  @doc false
  def to_elements(%__MODULE__{title: nil, description: nil}) do
    raise ArgumentError, message: "either title or description have to be set"
  end

  def to_elements(%__MODULE__{} = item) do
    elements =
      item
      |> Map.from_struct()
      |> Stream.filter(fn {_key, value} -> value != nil end)
      |> Enum.map(&build_element/1)

    element(:item, elements)
  end

  defp build_element({:link, url}) do
    element(:link, url_to_string(url))
  end

  defp build_element({:author, author}) do
    Author.to_elements(author)
  end

  defp build_element({:category, category}) when is_list(category) do
    Enum.map(category, &Category.to_elements/1)
  end

  defp build_element({:category, category}) do
    Category.to_elements(category)
  end

  defp build_element({:comments, url}) do
    element(:comments, url_to_string(url))
  end

  defp build_element({:enclosure, enclosure}) do
    Enclosure.to_elements(enclosure)
  end

  defp build_element({:guid, guid}) do
    Guid.to_elements(guid)
  end

  defp build_element({:pub_date, pub_date}) do
    element(:pubDate, datetime_to_rfc1123(pub_date))
  end

  defp build_element({:source, source}) do
    Source.to_elements(source)
  end

  # default case
  defp build_element({key, value}) do
    element(key, value)
  end
end
