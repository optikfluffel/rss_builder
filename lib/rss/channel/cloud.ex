defmodule RSS.Channel.Cloud do
  @moduledoc """
  When creating a Cloud struct, every field is required.

      cloud = %RSS.Channel.%Cloud{
        domain: "non.existing.tld",
        port: 1984,
        path: "/rpc",
        register_procedure: "pleasePingMe",
        protocol: "xml-rpc"
      }

  """

  import XmlBuilder, only: [element: 2]

  @enforce_keys [:domain, :port, :path, :register_procedure, :protocol]
  defstruct [:domain, :port, :path, :register_procedure, :protocol]

  @type t :: %__MODULE__{
          domain: String.t(),
          port: non_neg_integer(),
          path: String.t(),
          register_procedure: String.t(),
          protocol: String.t()
        }

  @doc false
  def to_elements(%__MODULE__{} = cloud) do
    attributes = %{
      domain: cloud.domain,
      port: cloud.port,
      path: cloud.path,
      registerProcedure: cloud.register_procedure,
      protocol: cloud.protocol
    }

    element(:cloud, attributes)
  end
end
