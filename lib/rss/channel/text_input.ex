defmodule RSS.Channel.TextInput do
  @moduledoc """
  When creating a TextInput struct, every field is required.

      text_input = %TextInput{
        title: "Submit Feedback",
        description: "Provide feedback regarding our awesome website.",
        name: "content",
        link: "https://non.existing.tld/feedback"
      }

  """

  import XmlBuilder, only: [element: 2]
  import RSS.Utilities, only: [url_to_string: 1]

  @enforce_keys [:title, :description, :name, :link]
  defstruct [:title, :description, :name, :link]

  @type t :: %__MODULE__{
          title: String.t(),
          description: String.t(),
          name: String.t(),
          link: URI.t() | String.t()
        }

  @doc false
  def to_elements(%__MODULE__{} = text_input) do
    element(:text_input, [
      element(:title, text_input.title),
      element(:description, text_input.description),
      element(:name, text_input.name),
      element(:link, url_to_string(text_input.link))
    ])
  end
end
