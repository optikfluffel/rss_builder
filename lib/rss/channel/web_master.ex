defmodule RSS.Channel.WebMaster do
  @moduledoc """
  When creating a WebMaster struct,
  the `:email` field is required,
  while the `:name` is optional.

      web_master = %RSS.Channel.WebMaster{
        email: "caerostris@non.existing.tld",
        name: "Caerostris darwini"
      }

  """

  import XmlBuilder, only: [element: 2]
  import RSS.Utilities, only: [email_string: 2]

  @enforce_keys [:email]
  defstruct [:email, :name]

  @type t :: %__MODULE__{
          email: String.t(),
          name: String.t()
        }

  @doc false
  def to_elements(%__MODULE__{name: name, email: email}) do
    element(:webMaster, email_string(email, name))
  end
end
