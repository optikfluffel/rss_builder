defmodule RSS.Channel.ManagingEditor do
  @moduledoc """
  When creating a ManagingEditor struct,
  the `:email` field is required,
  while the `:name` is optional.

      managing_editor = %RSS.Channel.ManagingEditor{
        email: "vaughan@non.existing.tld",
        name: "Dorothy Vaughan"
      }

  """

  import XmlBuilder, only: [element: 2]
  import RSS.Utilities, only: [email_string: 2]

  @enforce_keys [:email]
  defstruct [:email, :name]

  @type t :: %__MODULE__{
          email: String.t(),
          name: String.t()
        }

  @doc false
  def to_elements(%__MODULE__{name: name, email: email}) do
    element(:managingEditor, email_string(email, name))
  end
end
