defmodule RSS.Channel.Image do
  @moduledoc """
  ## Required

  When creating an Image struct, the `:url`, `:title` and `:link` fields are required.

      image = %Image{
        url: url,
        title: "An icon resembling a newspaper.",
        link: link
      }

  ## Optional

  ### `:width`

  Maximum of 144.

      image = %Image{
        # ...
        width: 120
      }

  ### `:height`

  Maximum of 400.

      image = %Image{
        # ...
        height: 240
      }

  ### `:description`

  Contains text that is included in the title attribute of the link
  formed around the image in the HTML rendering.

      image = %Image{
        # ...
        description: "A non existing News website"
      }

  """

  import XmlBuilder, only: [element: 2]
  import RSS.Utilities, only: [url_to_string: 1]

  @enforce_keys [:url, :title, :link]
  defstruct [:url, :title, :link, :width, :height, :description]

  @type t :: %__MODULE__{
          url: URI.t() | String.t(),
          title: String.t(),
          link: URI.t() | String.t(),
          width: pos_integer(),
          height: pos_integer(),
          description: String.t()
        }

  @doc false
  def to_elements(%__MODULE__{} = image) do
    elements =
      image
      |> Map.from_struct()
      |> Stream.filter(fn {_key, value} -> value != nil end)
      |> Enum.map(&build_element/1)

    element(:image, elements)
  end

  defp build_element({:url, url}) do
    element(:url, url_to_string(url))
  end

  defp build_element({:link, link}) do
    element(:link, url_to_string(link))
  end

  defp build_element({:width, width}) when is_integer(width) and width > 0 and width <= 144 do
    element(:width, Integer.to_string(width))
  end

  defp build_element({:width, _width}) do
    raise ArgumentError, message: "width for an Image should be 1-144 (pixel)"
  end

  defp build_element({:height, height})
       when is_integer(height) and height > 0 and height <= 400 do
    element(:height, Integer.to_string(height))
  end

  defp build_element({:height, _height}) do
    raise ArgumentError, message: "height for an Image should be 1-400 (pixel)"
  end

  # default case
  defp build_element({key, value}) do
    element(key, value)
  end
end
