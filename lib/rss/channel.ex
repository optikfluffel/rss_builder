defmodule RSS.Channel do
  @moduledoc """
  A Channel struct represents the `<channel>` and it's metadata in the RSS feed.

  ## Required

  When creating a Channel struct, the `:title`, `:link` and `:description` fields are required.

  The `:link` can either be a `URI` or a `String`.

      %RSS.Channel{
        title: "Philosopher's Stone",
        link: "https://philosophers.stone/news",
        description: "It is also called the elixir of life."
      }

  ## Recommended

  ### `:atom_link`

  An RSS feed can identify its own URL using an `<atom:link>` element.

  It can either be a `URI` or a `String`.

  This is recommended by the
  [RSS best practices](https://www.rssboard.org/rss-profile#namespace-elements-atom-link).

      channel = %RSS.Channel{
        # ...
        atom_link: "https://non.existing.tld/news/feed"
      }

  ## Optional

  There are also a lot of optional fields you can set:

  ### `:language`

  Language of the channels content

  See [rssboard.org](https://www.rssboard.org/rss-language-codes)
  and [w3.org](https://www.w3.org/TR/REC-html40/struct/dirlang.html#langcodes)
  for valid languages.

      channel = %RSS.Channel{
        # ...
        language: "en"
      }

  ### `:copyright`

  Copyright information

      channel = %RSS.Channel{
        # ...
        copyright: "© until eternity"
      }

  ### `:managing_editor`

  Person responsible for editorial content

  See `RSS.Channel.ManagingEditor` for details.

      channel = %RSS.Channel{
        # ...
        managing_editor: %ManagingEditor{email: "vaughan@non.existing.tld"}
      }

  ### `:web_master`

  Person responsible for technical issues relating to channel

  See `RSS.Channel.WebMaster` for details.

      channel = %RSS.Channel{
        # ...
        web_master: %WebMaster{email: "caerostris@non.existing.tld"}
      }

  ### `:pub_date`

  Publication date for the content in the channel, it can be a `NaiveDateTime` or `DateTime`.

      channel = %RSS.Channel{
        # ...
        pub_date: NaiveDateTime.utc_now()
      }

  ### `:last_build_date`

  Last time the content of the channel changed, can be a `NaiveDateTime` or `DateTime`

      channel = %RSS.Channel{
        # ...
        last_build_date: NaiveDateTime.utc_now()
      }

  ### `:category`

  One or more categories that the channel belongs to

      channel = %RSS.Channel{
        # ...
        category: "knitting"
      }

      # or

      channel = %RSS.Channel{
        # ...
        category: ["knitting", "fabric"]
      }

  ### `:generator`

  The program used to generate the channel

      channel = %RSS.Channel{
        # ...
        generator: "Fancy Phoenix CMS 0.1"
      }

  ### `:docs`

  A `URI` or `String`, that points to the documentation for the format used in the RSS file.

      channel = %RSS.Channel{
        # ...
        docs: "https://www.rssboard.org/rss-specification"
      }

  ### `:cloud`

  Allows processes to register with a cloud to be notified of updates to the channel.

  See `RSS.Channel.Cloud` for details.

      my_cloud = %Cloud{
        domain: "non.existing.tld",
        port: 1984,
        path: "/rpc",
        register_procedure: "pleasePingMe",
        protocol: "xml-rpc"
      }

      channel = %RSS.Channel{
        # ...
        cloud: my_cloud
      }

  ### `:ttl`

  Number of minutes that indicates how long a channel can be cached before refreshing.

      channel = %RSS.Channel{
        # ...
        ttl: 30
      }

  ### `:image`

  GIF, JPEG or PNG image that can be displayed with the channel.

  See `RSS.Channel.Image` for details.

      channel_image = %Image{
        url: "https://non.existing.tld/images/news.jpg",
        title: "An icon resembling a newspaper.",
        link: "https://non.existing.tld/news"
      }

      channel = %RSS.Channel{
        # ...
        image: channel_image
      }

  ### `:rating`

  TODO: 🤔

      channel = %RSS.Channel{
        # ...
        rating: "FSK 16"
      }

  ### `:text_input`

  Specifies a text input box that can be displayed with the channel.

  See `RSS.Channel.TextInput` for details.

      text_input = %TextInput{
        title: "Submit Feedback",
        description: "Provide feedback regarding our awesome website.",
        name: "content",
        link: "https://non.existing.tld/feedback"
      }

      channel = %RSS.Channel{
        # ...
        text_input: text_input
      }

  ### `:skip_hours`

  A list of numbers between 0 and 23, representing a time in GMT,
  when aggregators, if they support the feature, may not read the channel.

      channel = %RSS.Channel{
        # ...
        skip_hours: [0, 1, 2, 3, 4, 5, 6, 7, 8, 18, 19, 20, 21, 22, 23]
      }

  ### `:skip_days`

  A list of atoms representing the weekdays, when aggregators,
  if they support the feature, may not read the channel.

      channel = %RSS.Channel{
        # ...
        skip_days: [:saturday, :sunday]
      }

  """

  import XmlBuilder, only: [element: 2]
  import RSS.Utilities, only: [datetime_to_rfc1123: 1, url_to_string: 1]

  alias RSS.Channel.{Cloud, Image, ManagingEditor, TextInput, WebMaster}

  @enforce_keys [:title, :link, :description]
  defstruct [
    :title,
    :link,
    :description,
    :language,
    :copyright,
    :managing_editor,
    :web_master,
    :pub_date,
    :last_build_date,
    :category,
    :generator,
    :docs,
    :cloud,
    :ttl,
    :image,
    :rating,
    :text_input,
    :skip_hours,
    :skip_days,
    :atom_link
  ]

  @typedoc """
  Atoms representing each day of the week.
  """
  @type week_day :: :monday | :tuesday | :wednesday | :thursday | :friday | :saturday | :sunday

  @typedoc """
  A `%Channel{}` struct represents the `<channel>` and it's metadata in the RSS feed.
  The `:title`, `:link` and `:description` fields are required, all the others are optional.

  See `RSS.Channel` for more information.
  """
  @type t :: %__MODULE__{
          title: String.t(),
          link: URI.t() | String.t(),
          description: String.t(),
          language: String.t(),
          copyright: String.t(),
          managing_editor: ManagingEditor.t(),
          web_master: WebMaster.t(),
          pub_date: DateTime.t() | NaiveDateTime.t(),
          last_build_date: DateTime.t() | NaiveDateTime.t(),
          category: String.t() | [String.t()],
          generator: String.t(),
          docs: URI.t() | String.t(),
          cloud: Cloud.t(),
          ttl: non_neg_integer(),
          image: Image.t(),
          rating: String.t(),
          text_input: TextInput.t(),
          skip_hours: [non_neg_integer()],
          skip_days: [week_day],
          atom_link: URI.t() | String.t()
        }

  @doc false
  def to_elements(%__MODULE__{} = channel) do
    channel
    |> Map.from_struct()
    |> Stream.filter(fn {_key, value} -> value != nil end)
    |> Enum.map(&build_element/1)
  end

  defp build_element({:link, url}) do
    element(:link, url_to_string(url))
  end

  defp build_element({:managing_editor, managing_editor}) do
    ManagingEditor.to_elements(managing_editor)
  end

  defp build_element({:web_master, web_master}) do
    WebMaster.to_elements(web_master)
  end

  defp build_element({:pub_date, pub_date}) do
    element(:pubDate, datetime_to_rfc1123(pub_date))
  end

  defp build_element({:last_build_date, last_build_date}) do
    element(:lastBuildDate, datetime_to_rfc1123(last_build_date))
  end

  defp build_element({:category, categories}) when is_list(categories) do
    Enum.map(categories, fn category -> element(:category, category) end)
  end

  defp build_element({:docs, url}) do
    element(:docs, url_to_string(url))
  end

  defp build_element({:cloud, cloud}) do
    Cloud.to_elements(cloud)
  end

  defp build_element({:ttl, ttl}) do
    element(:ttl, Integer.to_string(ttl))
  end

  defp build_element({:image, image}) do
    Image.to_elements(image)
  end

  defp build_element({:text_input, text_input}) do
    TextInput.to_elements(text_input)
  end

  defp build_element({:skip_hours, skip_hours}) do
    hour_elements =
      Enum.map(skip_hours, fn hour ->
        element(:hour, Integer.to_string(hour))
      end)

    element(:skip_hours, hour_elements)
  end

  defp build_element({:skip_days, skip_days}) do
    day_elements =
      Enum.map(skip_days, fn day ->
        element(:day, day |> Atom.to_string() |> String.capitalize())
      end)

    element(:skip_days, day_elements)
  end

  defp build_element({:atom_link, feed_url}) do
    url = url_to_string(feed_url)

    element(:"atom:link", %{href: url, rel: "self", type: "application/rss+xml"})
  end

  # default case
  defp build_element({key, value}) do
    element(key, value)
  end
end
