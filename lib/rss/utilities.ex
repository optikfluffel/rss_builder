defmodule RSS.Utilities do
  @moduledoc """
  Internal Helper functions to format diffent things.
  """

  @rfc1123_format "%a, %d %b %Y %H:%M:%S %z"

  @doc """
  Takes a `DateTime` or `NaiveDateTime` and returns a `String` formatted
  accordig to the RSS spec.

  ## Examples

      iex> date_time = ~U[2020-11-05 15:30:01Z]
      iex> RSS.Utilities.datetime_to_rfc1123(date_time)
      "Thu, 05 Nov 2020 15:30:01 +0000"

      iex> naive_date_time = ~N[2020-11-05 15:31:02]
      iex> RSS.Utilities.datetime_to_rfc1123(naive_date_time)
      "Thu, 05 Nov 2020 15:31:02 +0000"
  """
  @spec datetime_to_rfc1123(DateTime.t() | NaiveDateTime.t()) :: String.t()
  def datetime_to_rfc1123(%DateTime{} = datetime) do
    Calendar.strftime(datetime, @rfc1123_format)
  end

  def datetime_to_rfc1123(%NaiveDateTime{} = naive_datetime) do
    naive_datetime
    |> DateTime.from_naive!("Etc/UTC")
    |> datetime_to_rfc1123()
  end

  @doc """
  When given a `%URI{}` calls `URI.to_string/1`, just passes `uri` through otherwise.

  ## Examples

      iex> RSS.Utilities.url_to_string("https://example.tld/posts/1234")
      "https://example.tld/posts/1234"

      iex> uri = URI.parse("https://example.tld/posts/1234")
      iex> RSS.Utilities.url_to_string(uri)
      "https://example.tld/posts/1234"
  """
  @spec url_to_string(URI.t() | String.t()) :: String.t()
  def url_to_string(%URI{} = uri), do: URI.to_string(uri)
  def url_to_string(url_str), do: url_str

  @doc """
  Helper to format emails.

  ## Examples

      iex> RSS.Utilities.email_string("ej_feinler@example.tld", nil)
      "ej_feinler@example.tld"

      iex> RSS.Utilities.email_string("ej_feinler@example.tld", "Elizabeth Jocelyn Feinler")
      "ej_feinler@example.tld (Elizabeth Jocelyn Feinler)"
  """
  @spec email_string(String.t(), nil | String.t()) :: String.t()
  def email_string(email, nil), do: email
  def email_string(email, name), do: "#{email} (#{name})"
end
