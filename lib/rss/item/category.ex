defmodule RSS.Item.Category do
  @moduledoc """
  When creating a Category struct,
  the `:title` field is required,
  while the `:domain` is optional.

      category = %Category{
        title: "Movie Reviews",
        domain: "https://non.existing.tld/news/tags/movie_reviews"
      }

  """

  import XmlBuilder, only: [element: 2, element: 3]
  import RSS.Utilities, only: [url_to_string: 1]

  @enforce_keys [:title]
  defstruct [:title, :domain]

  @type t :: %__MODULE__{
          title: String.t(),
          domain: URI.t() | String.t()
        }

  @doc false
  def to_elements(%__MODULE__{domain: nil, title: title}) do
    element(:category, title)
  end

  def to_elements(%__MODULE__{domain: domain, title: title}) do
    element(:category, %{domain: url_to_string(domain)}, title)
  end
end
