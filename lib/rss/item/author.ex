defmodule RSS.Item.Author do
  @moduledoc """
  When creating a Author struct,
  the `:email` field is required,
  while the `:name` is optional.

      author = %Author{
        email: "hamilton@non.existing.tld",
        name: "Margaret Heafield Hamilton"
      }

  """

  import XmlBuilder, only: [element: 2]
  import RSS.Utilities, only: [email_string: 2]

  @enforce_keys [:email]
  defstruct [:email, :name]

  @type t :: %__MODULE__{
          email: String.t(),
          name: String.t()
        }

  @doc false
  def to_elements(%__MODULE__{name: name, email: email}) do
    element(:author, email_string(email, name))
  end
end
