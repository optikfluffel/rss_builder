defmodule RSS.Item.Source do
  @moduledoc """
  When creating a Source struct,
  the `:url` field is required,
  while the `:title` is optional.

      source = %Source{
        url: "https://another.non.existing.tld/feed.xml",
        title: "A completely different Realm"
      }

  """

  import XmlBuilder, only: [element: 3]
  import RSS.Utilities, only: [url_to_string: 1]

  @enforce_keys [:url]
  defstruct [:url, :title]

  @type t :: %__MODULE__{
          url: URI.t() | String.t(),
          title: String.t()
        }

  @doc false
  def to_elements(%__MODULE__{title: title, url: url}) do
    element(:source, %{url: url_to_string(url)}, title)
  end
end
