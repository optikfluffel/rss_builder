defmodule RSS.Item.Enclosure do
  @moduledoc """
  When creating a Enclosure struct, all three fields are required.

      enclosure = %Enclosure{
        url: "https://non.existing.tld/news/spoken/nullnummer.mp3",
        length: 10_240,
        type: "audio/mpeg"
      }

  """

  import XmlBuilder, only: [element: 2]
  import RSS.Utilities, only: [url_to_string: 1]

  @enforce_keys [:url, :length, :type]
  defstruct [:url, :length, :type]

  @type t :: %__MODULE__{
          url: URI.t() | String.t(),
          length: non_neg_integer(),
          type: String.t()
        }

  @doc false
  def to_elements(%__MODULE__{url: url, length: length, type: type}) do
    element(:enclosure, %{
      url: url_to_string(url),
      length: Integer.to_string(length),
      type: type
    })
  end
end
