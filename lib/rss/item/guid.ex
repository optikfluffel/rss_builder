defmodule RSS.Item.Guid do
  @moduledoc """
  When creating a Author struct,
  the `:value` field is required and can be either a `URI` or a `String`,
  while the `:is_perma_link` boolean is optional and true by default.

      %Guid{
        value: "https://non.existing.tld/news/32849238749",
        is_perma_link: true
      }

  """

  import XmlBuilder, only: [element: 2, element: 3]

  @enforce_keys [:value]
  defstruct [:value, is_perma_link: true]

  @type t :: %__MODULE__{
          value: URI.t() | String.t(),
          is_perma_link: boolean()
        }

  @doc false
  def to_elements(%__MODULE__{is_perma_link: true, value: value}) do
    element(:guid, %{isPermaLink: true}, value)
  end

  def to_elements(%__MODULE__{value: value}) do
    element(:guid, value)
  end
end
