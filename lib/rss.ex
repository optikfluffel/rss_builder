defmodule RSS do
  @moduledoc """
  ## Basic Usage

  First prepare a `RSS.Channel` struct with at least
  some basic information about the RSS Channel.

      channel = %RSS.Channel{
        title: "Philosopher's Stone",
        link: "https://philosophers.stone/news",
        description: "It is also called the elixir of life."
      }

  Then prepare a list of `RSS.Item` structs with at a least a title.

      items = [
        %RSS.Item{title: "Transmute Base Metals Into Gold or Silver"},
        %RSS.Item{title: "Heal All Forms of Illness"},
        %RSS.Item{title: "Reviving of Dead Plants"}
      ]

  At last generate a binary containing the xml feed using the `feed/2` function.

      {:ok, feed_xml} = RSS.feed(channel, items)

  If you want a more minimal output, without all the indentation
  you can also include a `:no_indent` option.

      {:ok, without_indentation} = RSS.feed(channel, items, :no_indent)

  ## Usage with Phoenix

  First add a `get` route to your Phoenix Router.

      defmodule MyAppWeb.Router do
        # ...

        scope "/", MyAppWeb do
          pipe_through :browser

          get "/", PageController, :index

          # --> the new route for your rss feed
          get "/feed", FeedController, :index
        end
      end


  Then add a `FeedController` containing an `index/2` function to handle the request.
  In there the `RSS.Channel` struct and a list of `RSS.Item` structs are put together
  and are then used to generate and send the xml content as the response.

      defmodule MyAppWeb.FeedController do
        use MyAppWeb, :controller

        alias MyApp.Blog
        alias MyApp.Blog.Post
        alias RSS.{Channel, Item}

        def index(conn, _params) do
          # prepare Channel struct
          channel = %Channel{
            language: "en",
            title: "My little Blog",
            description: "This is an example weblog.",
            link: url(~p"/"),
            atom_link: url(~p"/feed")
          }

          # get all blog posts and prepare a list of Item structs
          posts = Blog.list_posts()
          items = Enum.map(posts, &post_to_rss_item(conn, &1))

          # generate xml feed
          {:ok, xml_content} = RSS.feed(channel, items)

          # send response
          conn
          |> put_resp_header("content-type", "application/rss+xml")
          |> send_resp(200, xml_content)
        end

        # helper function to make an RSS.Item from a Blog.Post
        defp post_to_rss_item(conn, %Post{} = post) do
          %Item{
            title: post.title,
            link: url(~p"/posts/\#{post.id}"),
            description: post.summary,
            pub_date: post.published_at
          }
        end
      end

  For [RSS Autodiscovery](https://www.rssboard.org/rss-autodiscovery)
  add a `<link rel="alternate">` containing your feed url to the `<head>` of your root layout (`root.html.heex` by default):

      <link rel="alternate" type="application/rss+xml" title="My RSS Feed" href={~p"/feed"}>
  """

  import XmlBuilder, only: [document: 3, element: 2, generate: 2]

  alias RSS.{Channel, Item}

  @rss_attributes %{version: "2.0", "xmlns:atom": "http://www.w3.org/2005/Atom"}

  @doc """
  Returns a `binary` containing the generated RSS feed xml content
  from the given `RSS.Channel` struct and a list of `RSS.Item` structs.

  ## Examples

      iex> RSS.feed(channel, items)
      {:ok, xml_content}

      iex> RSS.feed(channel, items, :no_indent)
      {:ok, xml_content_without_indentation}

  """
  @spec feed(Channel.t(), [Item.t()], :no_indent | :indent) :: {:ok, binary}
  def feed(%Channel{} = channel, items, format \\ :indent) do
    item_elements = Enum.map(items, &Item.to_elements/1)
    inner_channel_elements = Channel.to_elements(channel)

    channel_element = element(:channel, inner_channel_elements ++ item_elements)
    document = document(:rss, @rss_attributes, [channel_element])

    case format do
      :no_indent -> {:ok, generate(document, format: :none)}
      :indent -> {:ok, generate(document, format: :indent)}
    end
  end
end
